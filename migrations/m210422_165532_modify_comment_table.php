<?php

use yii\db\Migration;

/**
 * Class m210422_165532_modify_comment_table
 */
class m210422_165532_modify_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->addColumn(\app\models\Comment::tableName(), 'date', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210422_165532_modify_comment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210422_165532_modify_comment_table cannot be reverted.\n";

        return false;
    }
    */
}
